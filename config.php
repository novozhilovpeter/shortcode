<?php 

if (!(version_compare(PHP_VERSION, '5.6.0') >= 0)) {
	die('Unsupported. You must use php version > 5.6.0.');
}		

define('DIR_APPLICATION',__DIR__);