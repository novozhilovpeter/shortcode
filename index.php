<?php 

/****
	* @requirements:
	* @task: реализовать short uri converter
	* backend: php, .htaccess, 
	* реализация shourt uri: getShortURI->genShortURI
	* класс находится в /app/classes/general.php
	* реализует short uri вида от 5 символов a-z в разных регистрах
	* по размерности сохраненного массива, разный регистр (случайным образом) спасает от подбора предыдущих uri
	* массив сохраняется в serialize файл uri.txt (о базе в задании ничего не сказано, данный файл закрыт .htaccess)
	* использованы: классы, interface, traits, try/catch/throw, final, protected, public и т.п.
	* проверки: filter_var, filter_var | FILTER_VALIDATE_URL, gethostbyname 
	* front: чистый ecma script и реализация ajax, плюс проверка, поддерживается ли apache + .htaccess
	* если не поддерживается, то ссылка будет вида домен/?__route__=shortcode
	* в config.php реализована проверка php версии выше 5.6.0, иначе работать не будет
	* html5, css, flexbox + phpinfo
	* @test: tests.php
	* @supported: ie, ff, chrome, opera last
	* @todo: backend: шаблонизатор, mvc, frontend: подстановка протоколов,  регулярные html5 выражения для проверки
****/
	
try{
	
	require __DIR__.'/config.php';
	require DIR_APPLICATION.'/app/classes/general.php';
	
	$oRedirect = new getShortURI(DIR_APPLICATION.'/files/uri.txt');
	
	if(isset($_REQUEST['test']) 
		&& !empty($_REQUEST['test'])) {
		
		if(!headers_sent()){
			header('Content-type: application/json');
		}
		
		echo json_encode(array('result' => true));
		die();
		
	}
	
	if(isset($_REQUEST['sendlink']) 
		&& !empty($_REQUEST['sendlink'])) {
		$sUri = $oRedirect->setRedirect($_REQUEST['sendlink']);
		
		if(!headers_sent()){
			header('Content-type: application/json');
		}
		
		echo json_encode(array('result' => $sUri));
		die();
		
		
	}	
	
	if(isset($_REQUEST['_route_']) 
		&& !empty($_REQUEST['_route_'])){
		$oRedirect->getRedirect($_REQUEST['_route_']);
		die();
	}	
		

} catch(Exception $e) {
	
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) 
		&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	
		if(!headers_sent()){
			header('Content-type: application/json');
		}
		
		echo json_encode(array('error' => $e->getMessage()));
		die();
		
		
	} else {
		
		echo $e->getMessage(); 
		die();
		
	}
	
}

?>
<html>
<head>
	<title>Unique url</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="app/assets/css/style.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
</head>
<body>
	<form id="sendform">
		<div id="sendresult">
		</div>
		<div class="sendlink">
			<input type="text" placeholder="Enter link (@example: https://yandex.ru)" id="sendlink" name="sendlink" />
			<button>Get link</button>
		</div>
	</form>
	<script src="app/assets/js/script.js"></script>
	<div id="hide-mobile">
		<?php echo phpinfo(); ?>
	</div>
</body>
</html>
