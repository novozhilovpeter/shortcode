<?php 

/****
	* @task: реализовать short uri converter
	* @test: рандомом берется один из 5 урлов, подставляются, и получается код short hand, если проходит все проверки
****/
	

try{
	
	require __DIR__.'/config.php';
	require DIR_APPLICATION.'/app/classes/general.php';
	
	$oRedirect = new getShortURI(DIR_APPLICATION.'/files/uri.txt');
	
	$aLinks = array(
		'https://google.com.ua/',
		'https://yandex.ru',
		'https://google',
		'google.com.ua',
		'https://gmail.com'
	);
	
	$iRand = mt_rand(0,sizeof($aLinks)-1);
	$_REQUEST['sendlink'] = $aLinks[$iRand];

	echo 'full uri: '.$_REQUEST['sendlink'].' - ';
	$sUri = $oRedirect->setRedirect($_REQUEST['sendlink']);
	echo 'short uri: '.$sUri;
	die();
		

} catch(Exception $e) {
	
	echo $e->getMessage(); 
	die();
	
}
