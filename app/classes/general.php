<?php 

if(!defined('DIR_APPLICATION')) die();

	interface getShortURIBase{
		public function getRedirect($route);
		public function setRedirect($sendlink);
	}

	trait workPath{
		
		protected $aURIs = array();
		protected $sPath = '';
		
		public function __construct($sPath){
			
			$this->sPath = $sPath;
			$this->checkFile();
		}
		
		private function setURI($sUri,$sCode){
			
			$this->aURIs[$sUri] = $sCode;
			$this->writeURI();
		}
		
		private function checkShortURI($sShort){
			$this->checkFile();
			
			if(empty($this->aURIs)) {
				$this->getURIs();
			}
			
			$aCheck = array_flip($this->aURIs);
			
			return isset($aCheck[$sShort]) ? $aCheck[$sShort] : '';	
		}
		
		private function checkURI($sUri){
			
			$this->checkFile();
			
			if(empty($this->aURIs)) {
				$this->getURIs();
			}	
			
			return isset($this->aURIs[$sUri]) ? $this->aURIs[$sUri] : '';
		
		}
		
		private function writeURI(){
			
			$this->checkFile();
			
			if(empty($this->aURIs))
				$this->getURIs();
			
			file_put_contents($this->sPath,serialize($this->aURIs));
			
		}
		
		private function checkFile(){
			
			if(!file_exists($this->sPath)){
				file_put_contents($this->sPath,'');
			}
			
			if(!is_writable(dirname($this->sPath))){
				@chmod(dirname($this->sPath),0775);
			}
			
			if(!is_writable(dirname($this->sPath))){
				@chmod(dirname($this->sPath),0777);
			}
			
			if(file_exists($this->sPath)){
			
			} else {
				throw new Exception('Please, anable access for write to the directory '.dirname($this->sPath));
			}
			
		}
		
		private function getURIs(){
			
			$suris = file_get_contents($this->sPath); 	
			
			if($suris) {
				$auris = unserialize($suris);
				$this->aURIs = $auris;
			} 
			
			if(!is_array($auris)) {
				$auris = $this->aURIs = array();
			}
			
			return $auris;
			
		}
		
	}

	class getShortURI implements getShortURIBase{
		
		use workPath;
		
		public final function getRedirect($route){
			
			if(isset($route) && !empty($route)){
				
				$sRoute = filter_var($route);
				$sRoute = preg_replace('~[^a-z]~i','',$sRoute);
				$ssUri = $this->checkShortURI($sRoute);
				
				if($ssUri){
					
					if(!headers_sent()) {
						header('Location: '.$ssUri);
						die();
					}
					
				} else {
					throw new Exception('Your uri is not found.');
				}
				
			} else {
				
				throw new Exception('Your uri is empty. Check uri .');
				
			}
		
		}
		
		public final function setRedirect($sendlink){
			
			if(isset($sendlink) 
				&& !empty($sendlink)) {
				
				$sSendlink = filter_var($sendlink,FILTER_VALIDATE_URL);
				
				if(!empty($sSendlink)) {
					
					$aSendlink = parse_url($sSendlink);
					
					if(isset($aSendlink['host']) 
						&& !empty($aSendlink['host'])){
							
						$sAddr = gethostbyname($aSendlink['host']);
						
						if(filter_var($sAddr,FILTER_VALIDATE_IP)) {
							
							
							$sSendlink = trim($sSendlink);
							$sSendlink = rtrim($sSendlink,'/');
							
							$ssUri = $this->checkURI($sSendlink);
							
							if(!$ssUri) {
								
								$ssUri = $this->genShortURI();
								$this->setURI($sSendlink,$ssUri);
								
							}
							
							return $ssUri;
						
						}  else {
							
							throw new Exception('Can\'t get host ip address. URI '.$sSendlink.' is invalid or not working now.');
						}

					
					} else {
						
						throw new Exception('Can\'t get host ip address. URI '.$sSendlink.' is invalid or not working now.');
					}
					
							
					
				} else {
					
					throw new Exception('Your uri is wrong. Check uri .');
				
				}
				
			} else {
					
				throw new Exception('Your uri is wrong. Check uri .');
				
			}
		
		}
		
		public final function genShortURI(){
			
			$aStr = '';
			$iSizeof = sizeof($this->aURIs);
			
			if($iSizeof < 99999){
				$iSizeof = str_pad($iSizeof,5,0,STR_PAD_LEFT);
			}
			
			$aRange = range('a', 'z');
			
			for($i = 0; $i < strlen($iSizeof); $i++){
				
				$bStrToUpper = mt_rand(0,1);
				$aStr .= $bStrToUpper ? strtoupper($aRange[$iSizeof[$i]]) : $aRange[$iSizeof[$i]];	
				
			}
			
			return $aStr;
			
		}
		
	}
	