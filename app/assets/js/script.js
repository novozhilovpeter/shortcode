function isValidHttpUrl(string) {
		  let url;
		  
		  try {
			url = new URL(string);
		  } catch (e) {
			return false;  
		  }

		  return url.protocol === "http:" || url.protocol === "https:";
		}
		
		
		function ajaxCheckDomain(link,re)
		{
			var xmlHttp = new XMLHttpRequest();  
			xmlHttp.onreadystatechange = function()  
			{		
					if(xmlHttp.readyState === 4)  
					{
						var pUri = location.protocol + '//' + location.host + '' + location.pathname + '?_route_=' +re["result"] + '';
								
						if(xmlHttp.status === 200){
							
							var rde = JSON.parse(xmlHttp.responseText);  
							
							if(rde && rde['result']){
								var pUri = location.protocol + '//' + location.host + '' + location.pathname + re["result"] + '/';
								document.getElementById('sendresult').innerHTML = 'Copy Your short uri: <a href="javascript:prompt(\'Copy Your short uri:\',\''+pUri+'\');">' + pUri + '</a>';
								document.getElementById('sendform').className = 'result';
								return ;
							}
							
						}
						
						
						var pUri = location.protocol + '//' + location.host + '' + location.pathname + '?_route_=' +re["result"] + '';
						document.getElementById('sendresult').innerHTML = 'Copy Your short uri: <a href="javascript:prompt(\'Copy Your short uri:\',\''+pUri+'\');">' + pUri + '</a>';
						document.getElementById('sendform').className = 'result';
						return ;
						
						
					}

			}
				
			xmlHttp.open("GET", link);  
			xmlHttp.send(); 
			 
		}
		
		function ajaxRequest(link)
		{
			var xmlHttp = new XMLHttpRequest();  
			xmlHttp.onreadystatechange = function()  
			{
				   if(xmlHttp.readyState === 4 && xmlHttp.status === 200)  
				   {
					   var re = JSON.parse(xmlHttp.responseText);  
					
					   document.getElementById('sendform').className = '';
						
					   if(re["result"])
					   {	
							var pUri = location.protocol + '//' + location.host + '' + location.pathname + re["result"] + '/?test=1';
							ajaxCheckDomain(pUri,re);
							
					   } else {
							
							if(re["error"]){			
								document.getElementById('sendform').className = 'invalid';
								document.getElementById('sendresult').innerHTML = re["error"];
							}
					   }
				   }

			}
				
			xmlHttp.open("GET", link);  
			xmlHttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");
			xmlHttp.send(); 

		}
		
		var sForm = document.getElementById('sendform');
		if(sForm){
			sForm.addEventListener('submit',function(event){
				
				document.getElementById('sendform').className = '';
				
				var sLink = document.getElementById('sendlink').value;
				
				sLink = sLink.trim();
				
				if(isValidHttpUrl(sLink)) {
						
					var lurl = location.protocol + '//' + location.host + '' + location.pathname + '/?sendlink='+sLink;
					ajaxRequest(lurl);
					
				} else {
					document.getElementById('sendresult').innerHTML = 'Please, enter valid url, which contain\s protocol and hostname, for example: https://google.com';
					document.getElementById('sendform').className = 'invalid';
					document.getElementById('sendlink').focus();
				}
				
				event.preventDefault();
				return false;
			});
		}